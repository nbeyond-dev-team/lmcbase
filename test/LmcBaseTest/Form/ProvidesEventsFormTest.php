<?php
namespace LmcBaseTest\Form;

use PHPUnit_Framework_TestCase;
use LmcBase\Form\ProvidesEventsForm;
use Laminas\EventManager\EventManager;

class ProvidesEventsFormTest extends PHPUnit_Framework_TestCase
{
    public function setup()
    {
        $this->form = new ProvidesEventsForm;
    }

    public function testGetEventManagerSetsDefaultIdentifiers()
    {
        $em = $this->form->getEventManager();
        $this->assertInstanceOf('Laminas\EventManager\EventManager', $em);
        $this->assertContains('LmcBase\Form\ProvidesEventsForm', $em->getIdentifiers());
    }

    public function testSetEventManagerWorks()
    {
        $em = new EventManager();
        $this->form->setEventManager($em);
        $this->assertSame($this->form->getEventManager(), $em);
    }
}

