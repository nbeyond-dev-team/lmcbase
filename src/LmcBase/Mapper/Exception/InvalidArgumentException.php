<?php

namespace LmcBase\Mapper\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{}